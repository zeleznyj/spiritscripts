cimport cython


import numpy as np
cimport numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def cython_downsample(np.ndarray[np.float64_t,ndim=2] L_pos,
                      np.ndarray[np.float64_t,ndim=2] L_vecs,
                      np.ndarray[np.float64_t,ndim=1] bounds_min,
                      np.ndarray[np.float64_t,ndim=1] bounds_max,
                      np.ndarray[np.int_t,ndim=1] grid):

    cdef int n_L = grid[0] * grid[1] * grid[2]
    cdef np.ndarray[np.float64_t,ndim=2] new_pos = np.zeros((n_L,3),dtype=np.float64)
    cdef np.ndarray[np.float64_t,ndim=2] new_L = np.zeros((n_L,3),dtype=np.float64)
    cdef np.ndarray[np.float64_t,ndim=1] new_pos_i = np.zeros(3,dtype=np.float64)
    cdef np.ndarray[np.float64_t,ndim=1] bin_length = np.zeros(3,dtype=np.float64)
    cdef np.ndarray[np.int_t,ndim=1] n_i = np.zeros(3,dtype=np.int_)
    cdef np.ndarray[np.int_t,ndim=1] bin_nLs = np.zeros(n_L,dtype=np.int_)
    cdef int i,l,L_i
    cdef int n_L_old = L_pos.shape[0]
    cdef bint is_in

    for i in range(3):
        bin_length[i] = (bounds_max[i]-bounds_min[i])/grid[i]

    for i in range(n_L_old):
        is_in = True
        for l in range(3):
            if L_pos[i,l] < bounds_min[l]:
                is_in = False
                break
            if L_pos[i,l] > bounds_max[l]:
                is_in = False
                break
        if not is_in:
            continue
        else:
            for l in range(3):
                n_i[l] = int( (L_pos[i,l] - bounds_min[l]) / bin_length[l])
            L_i = n_i[0] * (grid[1]*grid[2]) + n_i[1] * grid[2] + n_i[2]
            for l in range(3):
                new_pos_i[l] = n_i[l]*bin_length[l] + bounds_min[l]
            new_pos[L_i,:] = new_pos_i
            new_L[L_i,:] = new_L[L_i,:] + L_vecs[i,:]
            bin_nLs[L_i] += 1
    for i in range(n_L):
        new_L[i,:] = new_L[i,:]/bin_nLs[i]
    return new_pos,new_L

