import setuptools   
from Cython.Build import cythonize

setuptools.setup(
        name='spiritscripts',
        version='0.1.1',
        packages=['spiritscripts'],
        scripts=['spiritscripts/plot_L.py'],
        ext_modules= cythonize('spiritscripts/cdownsample.pyx'),
        install_requires = [
            'matplotlib',
            'numpy',
            'scipy',
            'cython',
            'json_tricks'
            ]
        )
