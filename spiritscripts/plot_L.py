#!/usr/bin/env python
import matplotlib.pyplot as plt
from matplotlib import animation
from mpl_toolkits.mplot3d import axes3d
import sys
import numpy as np
from scipy.spatial import distance
import argparse
from math import pi
from numpy.linalg import norm
from functools import partial
import datetime
import os
import os.path
import hashlib
import pickle
import json_tricks

from spirit import state,io,quantities,log,geometry,system
from spiritscripts.cumnas import atom_type
from spiritscripts.player import Player

import pyximport; pyximport.install()
from spiritscripts.cdownsample import cython_downsample

def rebin(array,grid):
    #from https://scipython.com/blog/binning-a-2d-array-in-numpy/
    shape = array.shape
    new_array = array.reshape((grid[0],shape[0]//grid[0],
                        grid[1],shape[1]//grid[1],
                        grid[2],shape[2]//grid[2])).mean(0).mean(1).mean(2)
    return new_array

class CacheParams:
    def __init__(self,filename,grid,zoom,plot_M):
        self.filename=filename
        self.grid=grid
        self.zoom=zoom
        self.plot_M = plot_M
    def __str__(self):
        out = ''
        out += 'file: {0}'.format(self.filename)
        out += '\n'
        out += 'grid: {0}'.format(self.grid)
        out += '\n'
        out += 'zoom: {0}'.format(self.zoom)
        out += '\n'
        out += 'plot_M: {0}'.format(self.plot_M)
        out += '\n'
        return out

class DownsampleCache:

    def __init__(self,folder,method='json'):
        self.folder = folder
        self.method = method

    def get_cachename(self,params):
        s = hashlib.sha3_512()
        hash_string = params.filename+str(params.grid)+str(params.zoom)
        if params.plot_M:
            hash_string += ':plot_M:'
        hash_string = hash_string.encode()
        s.update(hash_string)
        cachename = s.hexdigest()
        return cachename

    def dump(self,params,value):
        cachename = self.get_cachename(params)
        if self.method == 'pickle':
            with open(self.folder+'/'+cachename,'wb') as f:
                pickle.dump(value,f)
        elif self.method == 'json':
            with open(self.folder+'/'+cachename,'w') as f:
                json_tricks.dump(value,f)
        else:
            raise Exception('Unknown method')
        with open(self.folder+'/'+cachename+'.params','w') as f:
            print(params,file=f)

    def load(self,params):
        cachename = self.get_cachename(params)
        if self.method == 'pickle':
            with open(self.folder+'/'+cachename,'rb') as f:
                value = pickle.load(f)
        elif self.method == 'json':
            with open(self.folder+'/'+cachename,'r') as f:
                value = json_tricks.load(f)
        else:
            raise Exception('Unknown method')

        return value

    def is_cached(self,params):
        cachename = self.get_cachename(params)
        if os.path.isfile(self.folder+'/'+cachename):
            return True
        else:
            return False

    def remove(self,params):
        cachename = self.get_cachename(params)
        if self.is_cached(params):
            os.remove(self.folder+'/'+cachename)
            if os.path.isfile(self.folder+'/'+cachename+'.params'):
                os.remove(self.folder+'/'+cachename+'.params')

def set_axes_equal(ax):
    #from https://stackoverflow.com/questions/13685386/matplotlib-equal-unit-length-with-equal-aspect-ratio-z-axis-is-not-equal-to
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

def pos_cell(b_vecs,pos):
    R = [pos[i] // b_vecs[i][i] for i in range(3)]
    return R


def get_L_field(p_state):
    positions = geometry.get_positions(p_state)
    spin_directions = system.get_spin_directions(p_state)
    L_pos = []
    L_vecs = []
    M_vecs = []

    for i,pos in enumerate(positions):
        at = atom_type(pos)
        spin = np.array(spin_directions[i])
        if at == 0:
            L_pos.append(np.array(pos)) 
            L_vecs.append(spin)
            M_vecs.append(spin)
        if at == 1:
            L_vecs[i//2] = L_vecs[i//2] - spin
            M_vecs[i//2] = M_vecs[i//2] + spin
    return np.array(L_pos),np.array(L_vecs),np.array(M_vecs)

def downsample(p_state,L_pos,L_vecs,grid=None,zoom=None,method=0):

    method = 1

    prec=1e-5

    if grid is None and zoom is None:
        return L_pos,L_vecs,None

    b_vecs = geometry.get_bravais_vectors(p_state)
    #bounds_min, bounds_max = geometry.get_bounds(p_state)
    n_cells = geometry.get_n_cells(p_state)
    bounds_min = [0.0,0.0,0.0]
    bounds_max = [n_cells[i] * b_vecs[i][i] for i in range(3)]

    bounds_min2 = []
    bounds_max2 = []
    if zoom is not None:
        for i,z in enumerate(zoom):
            if z[0] is not None:
                bounds_min2.append(z[0])
            else:
                bounds_min2.append(bounds_min[i])
            if z[1] is not None:
                bounds_max2.append(z[1])
            else:
                bounds_max2.append(bounds_max[i])
        bounds_min = bounds_min2
        bounds_max = bounds_max2

    if grid is not None:
        bin_length = [(bounds_max[l]-bounds_min[l])/grid[l] for l in range(3)]

    old_alg = False
    if old_alg:
        if zoom is not None:
            raise Exception('Not implemented.')
        new_pos = []
        new_L = []
        bin_nLs = []
        for i in range(grid[0]):
            for j in range(grid[1]):
                for k in range(grid[2]):
                    ind=(i,j,k)
                    bin_pos = [ind[l]*bin_length[l] + bounds_min[l] for l in range(3)]
                    new_pos.append(bin_pos)
                    bin_L = np.zeros(3)
                    bin_nL = 0
                    for l,pos in enumerate(L_pos):
                        is_in = True
                        for m in range(3):
                            if pos[m] < bin_pos[m] - prec:
                                is_in = False
                                break
                            if pos[m] >= bin_pos[m]+bin_length[m]-prec:
                                is_in = False
                                break
                        if is_in:
                            bin_L += L_vecs[l]
                            bin_nL += 1
                    if bin_nL > 0:
                        bin_L = bin_L / bin_nL
                        new_L.append(bin_L)
                    else:
                        new_L.append(None)
                    bin_nLs.append(bin_nL)
        return np.array(new_pos),np.array(new_L),bin_nLs
    else:
        if grid is not None:
            if method == 0:
                n_L = grid[0] * grid[1] * grid[2]
                new_pos = np.zeros((n_L,3))
                new_L = np.zeros((n_L,3))
                bin_nLs = np.zeros(n_L)
                for i,pos in enumerate(L_pos):
                    is_in = True
                    for l in range(3):
                        if pos[l] < bounds_min[l]:
                            is_in = False
                            break
                        if pos[l] > bounds_max[l]:
                            is_in = False
                            break
                    if not is_in:
                        continue
                    else:
                        n_i = [int( (pos[l] - bounds_min[l]) / bin_length[l]) for l in range(3)]
                        L_i = n_i[0] * (grid[1]*grid[2]) + n_i[1] * grid[2] + n_i[2]
                        new_pos[L_i,:] = np.array([n_i[l]*bin_length[l] + bounds_min[l] for l in range(3)])
                        new_L[L_i] = new_L[L_i] + L_vecs[i]
                        bin_nLs[L_i] += 1
                for i,L in enumerate(new_L):
                    new_L[i] = L/bin_nLs[i]
            elif method == 1:
                new_pos,new_L = cython_downsample(
                        L_pos,L_vecs,np.array(bounds_min),np.array(bounds_max),np.array(grid,dtype=np.int_))
                bin_nLs = None
        else:
            new_pos = []
            new_L = []
            for i,pos in enumerate(L_pos):
                new_pos.append(pos)
                new_L.append(L_vecs[i])
            new_pos = np.array(new_pos)
            new_L = np.array(new_L)
            bin_nLs = []
        return new_pos,new_L,bin_nLs


def plot_L_field(L_pos,L_vecs,scale=1):
    fig = plt.figure(figsize=plt.figaspect(1.0)*1.5)
    ax = fig.gca(projection='3d',proj_type = 'ortho')

    # Color by azimuthal angle
    c = np.arctan2(L_vecs[:,1], L_vecs[:,0])
    c=(c+pi)/(2*pi)
    # Flatten and normalize
    #c = (c.ravel() - c.min()) / c.ptp()
    # Repeat for each body line and two head lines
    c2 = []
    for ci in c:
        c2.append(ci)
        c2.append(ci)
        c2.append(ci)
    c = np.concatenate((c, np.repeat(c, 2)))
    # Colormap
    #c = plt.cm.twilight(c)
    c = plt.cm.hsv(c)

    dist = distance.euclidean(L_pos[0],L_pos[1])

    ax.quiver(L_pos[:,0],L_pos[:,1],L_pos[:,2],L_vecs[:,0],L_vecs[:,1],L_vecs[:,2],color=c,length=dist/3*scale)
    set_axes_equal(ax)
    plt.show()

def plot_L_field_2D(L_poss,L_vecss,filename,color='ip',animate=False,scale=None,zlayer=None,save=None,
        fps=5,
        bitrate=18000):
    n_files = len(filename)
    def plot_i(i,ax=None,Q=None,z=None,fig=None):
        L_pos = L_poss[i]
        L_vecs = L_vecss[i]
        devs = np.zeros(L_pos.shape[0])

        if z is not None:
            zlayers = list(set([pos[2] for pos in L_pos]))
            zlayers.sort()
            zlayer_val = zlayers[zlayer[z]]
            L_pos_z = []
            L_vecs_z = []
            for j,pos in enumerate(L_pos):
                if pos[2] == zlayer_val:
                    L_pos_z.append(pos)
                    L_vecs_z.append(L_vecs[j])
            L_pos = np.array(L_pos_z)
            L_vecs = np.array(L_vecs_z)

        for j,L in enumerate(L_vecs):
            devs[j] = abs(L[2] / norm(L))
        if np.max(devs) > 0.1:
            print('WARNING, large z component!') 
            print('maximum z component:', np.max(devs))
            print('average z component:', np.mean(devs))
        
        pos_z = [pos[2] for pos in L_pos]
        n_pos_z = len(set(pos_z))
        if n_pos_z > 1:
            print('WARNING, vectors have different z pos values')
            print(set(pos_z))
            print(len(L_pos))
        
        colors = {}
        # Color by azimuthal angle
        c = np.arctan2(L_vecs[:,1], L_vecs[:,0])
        c=(c+pi)/(2*pi)
        colors['ip'] = c
        c = []
        for L_i in L_vecs:
            c.append(np.arccos(L_i[2] / norm(L_i)))
        c = np.array(c) / pi
        colors['oop'] = c
        # Flatten and normalize
        #c = (c.ravel() - c.min()) / c.ptp()
        # Repeat for each body line and two head lines
        for cname in colors:
            c = colors[cname]
            c = np.concatenate((c, np.repeat(c, 2)))
            # Colormap
            #c = plt.cm.twilight(c)
            c = plt.cm.hsv(c)
            colors[cname] = c
        
        if not animate:
            print(ax)
            if not isinstance(ax,list):
                ax.set_title(filename[i])
                ax.quiver(L_pos[:,0],L_pos[:,1],L_vecs[:,0],L_vecs[:,1],color=colors[color],scale=scale)
                ax.set_aspect('equal')
            else:
                for i,axx in enumerate(ax):
                    colors_all = ['ip','oop']
                    axx.quiver(L_pos[:,0],L_pos[:,1],L_vecs[:,0],L_vecs[:,1],color=colors[colors_all[i]],scale=scale)
                    axx.set_aspect('equal')

        else:
            if Q is None:
                Q = []
                if isinstance(ax,list):
                    Q.append(ax[0].quiver(L_pos[:,0],L_pos[:,1],L_vecs[:,0],L_vecs[:,1],color=colors['ip'],scale=scale))
                    Q.append(ax[1].quiver(L_pos[:,0],L_pos[:,1],L_vecs[:,0],L_vecs[:,1],color=colors['oop'],scale=scale))
                else:
                    Q = ax.quiver(L_pos[:,0],L_pos[:,1],L_vecs[:,0],L_vecs[:,1],color=colors[color],scale=scale)
            else:
                if isinstance(Q,list):
                    Q[0].set_UVC(L_vecs[:,0],L_vecs[:,1])
                    Q[0].set_color(colors['ip'])
                    Q[1].set_UVC(L_vecs[:,0],L_vecs[:,1])
                    Q[1].set_color(colors['oop'])
                else:
                    Q.set_UVC(L_vecs[:,0],L_vecs[:,1])
                    Q.set_color(colors[color])
            if isinstance(ax,list):
                titles = ['in-plane color','out-of-plane color']
                for j,axx in enumerate(ax):
                    axx.set_title(titles[j])
                    axx.set_xlabel(r'$[\AA$]')
                    axx.set_ylabel(r'$[\AA]$')
                if fig is not None and save is None:
                    fig.suptitle(filename[i])
            else:
                ax.set_title(filename[i])
            if fig is not None:
                if z is not None:
                    fig.suptitle('z = {0}'.format(zlayer[z]))
            return Q

    def plot_z(i,ax=None,Q=None,file_i=0,fig=None):
        return plot_i(file_i,ax,Q,z=i,fig=fig)

    if not animate:
            figs = []
            axs = []
            if color != 'both':
                for i in range(n_files):
                    fig = plt.figure(i)
                    ax = fig.add_subplot(111)
                    plot_i(i)
            else:
                fig,ax = plt.subplots(n_files,2,figsize=(10,4.5*n_files),sharex=True,sharey=True)
                if n_files == 1:
                    plot_i(0,list(ax))
                    ax[0].set_xlabel(r'$[\AA$]')
                    ax[1].set_xlabel(r'$[\AA$]')
                    ax[0].set_ylabel(r'$[\AA$]')

                else:
                    for i in range(n_files):
                        plot_i(i,list(ax[i,:]))
                        ax[i,0].set_ylabel(r'$[\AA$]')
                        ax[n_files-1,0].set_xlabel(r'$[\AA$]')
                        ax[n_files-1,1].set_xlabel(r'$[\AA$]')
                plt.tight_layout()
                if save is not None:
                    fig.savefig(save)
                figs.append(fig)
                axs.append(ax)

    else:
        if save is None:
            hide_controls = False
        else:
            hide_controls = True
        #if scale is not None:
            #dist = distance.euclidean(L_poss[0][0],L_poss[0][1])
        #    scale = scale
        if zlayer is None:
            zstart = None
        else:
            zstart = 0
        if color != 'both':
            fig,ax  = plt.subplots()
            fig.set_size_inches(40,20)
            Q = plot_i(0,ax,z=zstart)
            ax.set_aspect('equal')
        else:
            fig,ax  = plt.subplots(1,2)
            fig.set_size_inches(40,20)
            ax = list(ax)
            Q = plot_i(0,ax,z=zstart,fig=fig)
            for axx in ax:
                axx.set_aspect('equal')
        if zlayer is None:
            ani = Player(fig, partial(plot_i,ax=ax,Q=Q), maxi=n_files-1,blit=False,hide_controls=hide_controls)
        else:
            if len(zlayer) == 1:
                ani = Player(fig, partial(plot_i,ax=ax,Q=Q,z=0,fig=fig), maxi=n_files-1,blit=False,hide_controls=hide_controls)
            else:
                ani = Player(fig, partial(plot_z,ax=ax,Q=Q,fig=fig),maxi=len(zlayer)-1,blit=False,hide_controls=hide_controls)

        #ani = animation.ArtistAnimation(fig_a,axs)

    if animate and save is not None:
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=fps, metadata=dict(artist='Me'), bitrate=bitrate)
        ani.save(save,writer=writer)

    plt.show()

if __name__ == "__main__":
    print(datetime.datetime.now().time(),'starting')
    parser = argparse.ArgumentParser()
    parser.add_argument('filename',nargs="+")
    parser.add_argument('-g','--grid',default=None)
    parser.add_argument('--scale',default=None)
    parser.add_argument('--2d',action="store_true",dest='twodim')
    parser.add_argument('--animate',action="store_true",dest='animate')
    parser.add_argument('--zoom',default=None)
    parser.add_argument('--color',default='ip')
    parser.add_argument('--rew-cache',action="store_true")
    parser.add_argument('-M',action="store_true")
    parser.add_argument('--zlayer',default=None)
    parser.add_argument('--cache-method',default='json')
    parser.add_argument('--save',default=None)
    parser.add_argument('--fps',default=5,type=int)
    parser.add_argument('--bitrate',default=18000,type=int)
    args = parser.parse_args()
    filename = args.filename
    scale = args.scale
    if scale is not None:
        scale = float(scale)
    grid = args.grid 
    twodim = args.twodim
    zoom = args.zoom
    if zoom is not None:
        zoom = zoom.split(',')
        zoom2 = []
        for z in zoom:
            if z == ':':
                zoom2.append((None,None))
            else:
                zoom2.append([float(x) for x in z.split(':')])
        zoom = zoom2
    if grid is not None:
        grid = [int(i) for i in grid.split(',')]
    if not twodim and len(filename) > 1:
        raise Exception('only one file allowed in the 3D mode')
    color = args.color
    animate = args.animate
    rew_cache = args.rew_cache
    plot_M = args.M
    if args.zlayer is None:
        zlayer = args.zlayer
    else:
        try:
            zlayer=[int(args.zlayer)]
        except:
            zlayer = args.zlayer.split(",")
            zlayer = [int(z) for z in zlayer]

    if args.cache_method == 'pickle':
        if not os.path.exists('downsamplecache'):
            os.makedirs('downsamplecache')
        cache=DownsampleCache('downsamplecache',method='pickle')
    elif args.cache_method == 'json':
        if not os.path.exists('downsamplecache_json'):
            os.makedirs('downsamplecache_json')
        cache=DownsampleCache('downsamplecache_json',method='json')
    if args.save is None:
        save = None
    else:
        save = str(args.save)
    fps = args.fps
    bitrate = args.bitrate

    with state.State("input/input.cfg",quiet=True) as p_state:
        try:
            log.set_output_to_file(p_state,False,0)
        except:
            log.set_outputToFile(p_state,False,0)
        log.set_output_to_console(p_state,True,1)
        
        n_files = len(filename)
        L_poss = []
        L_vecss = []
        for i in range(n_files):
            params = CacheParams(filename[i],grid,zoom,plot_M)
            print(filename[i],grid,zoom,plot_M)
            print(cache.get_cachename(params))
            #print(cache.is_cached(params))
            cache_loaded = False
            if not rew_cache and cache.is_cached(params):
                try:
                    print('reading from cache')
                    L_pos_d,L_vecs_d = cache.load(params)
                    print('cache loaded')
                    L_poss.append(L_pos_d)
                    L_vecss.append(L_vecs_d)
                    cache_loaded = True
                except Exception as e:
                    print('failed to read cache')
                    print(str(e))
                    cache.remove(params)

            if not cache_loaded:
                io.image_read(p_state,filename[i])
                print(datetime.datetime.now().time(),'State loaded')
                L_pos,L_vecs,M_vecs = get_L_field(p_state)
                print(datetime.datetime.now().time(),'L_field obtained')
                if not plot_M:
                    L_pos_d,L_vecs_d,bin_nLs = downsample(p_state,L_pos,L_vecs,grid=grid,zoom=zoom)
                else:
                    L_pos_d,L_vecs_d,bin_nLs = downsample(p_state,L_pos,M_vecs,grid=grid,zoom=zoom)
                print(datetime.datetime.now().time(),'Downsampling finished')
                print('writing to cache')

                cache.dump(params,(L_pos_d,L_vecs_d))
                L_poss.append(L_pos_d)
                L_vecss.append(L_vecs_d)

    if twodim:
        plot_L_field_2D(L_poss,L_vecss,filename,color=color,animate=animate,scale=scale,zlayer=zlayer,save=save,
                fps=fps,
                bitrate=bitrate)
    else:
        if scale is None:
            scale=1
        plot_L_field(L_poss[0],L_vecss[0],scale=scale)
