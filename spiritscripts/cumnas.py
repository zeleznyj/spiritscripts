def atom_type(position):
    z_comp = position[2]-int(position[2]/6.318)*6.318
    if abs(z_comp - 4.2317964) < 1e-4:
        at_type = 0
    elif abs(z_comp - 2.0862036) < 1e-4:
        at_type = 1
    else:
        raise Exception("Wrong z_comp")
    return at_type

